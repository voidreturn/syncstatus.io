import React from 'react'
import { inject, observer } from 'mobx-react'
import EthnodeStore from '../stores/ethnode'

@inject('ethnode')
@observer
export default class AddressField extends React.Component<{
  ethnode?: EthnodeStore
}> {
  state = {
    text: '',
    placeholder: '',
  }

  textFieldRef = React.createRef<any>()
  placeholderTimer: any

  onChange = (e: any) => this.setState({ text: e.target.value })

  componentDidMount() {
    // Auto select the text field on mount
    const timer = setInterval(() => {
      if (!this.textFieldRef.current) return
      this.textFieldRef.current.focus()
      clearTimeout(timer)
    }, 10)
    this.setState({
      placeholder: this.randomUrl(),
    })
    this.placeholderTimer = setInterval(() => {
      let placeholder = this.randomUrl()
      while (placeholder === this.state.placeholder) placeholder = this.randomUrl()
      this.setState({ placeholder })
    }, 3000)
  }

  componentWillUnmount() {
    clearInterval(this.placeholderTimer)
  }

  randomUrl() {
    const possibleUrls = [
      'ws://173.21.45.1:8546',
      'wss://node.url.com',
      'http://294.233.5.39:8545',
      'https://anothernode.url.io',
    ]
    const index = Math.floor(Math.random() * possibleUrls.length)
    return possibleUrls[index]
  }

  render() {
    return (
      <input
        ref={this.textFieldRef}
        style={{
          minWidth: 300,
          border: '1px solid #000',
          padding: 4,
        }}
        placeholder={this.props.ethnode.providerUrl || this.state.placeholder}
        type="text"
        onChange={this.onChange}
        onKeyDown={async (e: any) => {
          if (e.key === 'Enter') {
            await this.props.ethnode.loadSyncState(this.state.text)
          }
        }}
        value={this.state.text}
      />
    )
  }
}
