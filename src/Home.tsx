import React from 'react'
import AddressField from './components/AddressField'
import { inject, observer } from 'mobx-react'
import EthnodeStore from './stores/ethnode'
import idx from 'idx'
import { GitlabLogo, MoonIcon } from './components/Icons'

@inject('ethnode')
@observer
export default class Home extends React.Component<{
  ethnode?: EthnodeStore
}> {
  state = {
    syncState: '',
    minHeight: 0,
  }

  timer: any
  componentDidMount() {
    this.timer = setInterval(async () => {
      await this.props.ethnode.reload()
    }, 30 * 1000)
    this.updateMinHeight()
    window.addEventListener('resize', this.updateMinHeight)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateMinHeight)
  }

  updateMinHeight = () => {
    this.setState({
      minHeight: window.innerHeight - 20,
    })
  }

  setSyncingExample = async () => {
    await this.props.ethnode.loadSyncState('ws://homestead.makedco.io:8546')
  }

  setCompleteExample = async () => {
    await this.props.ethnode.loadSyncState('wss://homestead.makedco.io:444')
  }

  networkNameById = (id: number) => {
    switch (id) {
      case 1:
        return 'Homestead'
      case 3:
        return 'Ropsten'
      case 4:
        return 'Rinkeby'
      case 5:
        return 'Görli'
      case 42:
        return 'Kovan'
      case 61:
        return 'ETC Mainnet'
      case 62:
        return 'ETC Testnet'
      default:
        return 'Unknown Network'
    }
  }

  renderData = () => {
    const { ethnode, ethnode: { syncState } } = this.props
    const networkId = idx(syncState, (_) => _.networkId)
    return (
      <div style={{ margin: 8, maxWidth: 300, display: 'flex', flexDirection: 'column' }}>
        {ethnode.providerUrl}
        <div style={{ height: 10 }} />
        Network Id: {networkId} ({this.networkNameById(networkId)})
        <div style={{ height: 10 }} />
        Current Block: {idx(syncState, (_) => _.currentBlock)}
        {ethnode.isSyncing ? (
          <>
            <div style={{ height: 10 }} />
            Pulled States: {idx(syncState, (_) => _.pulledStates)}
          </>
        ) : null}
        <div style={{ height: 10 }} />
        Total Known States: {ethnode.approxTotalStatesByNetworkId[networkId]}
        <div style={{ height: 10 }} />
        Percent Complete: {ethnode.percentComplete}%
        {ethnode.isSyncing ? null : (
          <>
            <div style={{ height: 10 }} />
            <span style={{ color: 'green' }}>
              This node has completed syncing and is importing chain segments
            </span>
          </>
        )}
      </div>
    )
  }

  render() {
    const { ethnode } = this.props
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: this.state.minHeight,
        userSelect: 'none',
      }}>
        <div style={{
          border: '2px solid black',
          padding: 2,
          fontWeight: 'bold',
          fontSize: 19,
        }}>
          syncstatus
        </div>
        <div style={{ height: 20 }} />
        {ethnode.connected ? null : (
          <>
            <div style={{ maxWidth: 300 }}>
              A web app for quantifying trie state synchronization progress against a stored value.
              <div style={{ height: 10 }} />
              A progress bar for Ethereum node synchronization.
              <div style={{ height: 10 }} />
              Hopefully helps with{' '}
              <a href="https://github.com/ethereum/go-ethereum/issues/15616" target="_blank">
              this
              </a>.
            </div>
            <div style={{ height: 10 }} />
          </>
        )}
        {
          ethnode.connected ?
          this.renderData() :
          <MoonIcon style={{ width: 300 }} />
        }
        <div style={{ width: 300 }}>
          <AddressField />
          <div style={{ height: 8 }} />
          <div style={{ display: 'flex', justifyContent: 'space-around' }}>
            <button onClick={this.setSyncingExample}>Syncing Example</button>
            <button onClick={this.setCompleteExample}>Complete Example</button>
          </div>
        </div>
        <div style={{ flex: 1 }} />
        <div style={{ margin: 8 }}>
          <a href="https://gitlab.com/voidreturn/syncstatus.io" target="_blank">
            <GitlabLogo />
          </a>
        </div>
      </div>
    )
  }
}
