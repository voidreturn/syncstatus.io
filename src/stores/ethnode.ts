import { observable } from 'mobx'
import axios from 'axios'

interface SyncState {
  currentBlock: number
  knownStates?: number
  pulledStates?: number
  networkId: number
  isSyncing: boolean
}

export default class EthnodeStore {

  approxTotalStatesByNetworkId: { [key: number]: number} = {
    0: 0,
    1: 372000000,
    3: 82996738,
    4: 82996738,
  }

  @observable syncState?: SyncState
  @observable providerUrl: string = ''
  @observable connected: boolean = false

  get percentComplete() {
    const { syncState, approxTotalStatesByNetworkId, isSyncing } = this
    const { networkId } = syncState
    if (!isSyncing) return 100
    const approxStates = approxTotalStatesByNetworkId[+networkId]
    if (approxStates === 0) return 0
    // on some real shit, wtf
    const percent = +parseFloat('' + 100 * (syncState.pulledStates || 0) / approxStates).toFixed(2)
    return Math.min(percent, 100)
  }

  get isSyncing() {
    return this.syncState && this.syncState.isSyncing
  }

  async loadSyncState(url: string) {
    const { data } = await axios.get('https://syncstatusio.voidreturn.now.sh/api', {
      params: {
        url,
      },
    })
    this.providerUrl = url
    this.syncState = data
    this.connected = true
  }

  async reload() {
    if (!this.providerUrl) return
    const provider = this.providerUrl
    await this.loadSyncState(this.providerUrl)
    if (provider !== this.providerUrl) return
  }

  saveSyncState() {
    const { syncState } = this
    window.localStorage.setItem(this.providerUrl, JSON.stringify(syncState))
  }
}
