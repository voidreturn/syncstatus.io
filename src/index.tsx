import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import Ethnode from './stores/ethnode'

const stores = {
  ethnode: new Ethnode(),
}
const appDiv = document.getElementById('app')

document.body.style.fontFamily = 'helvetica'

ReactDOM.render(
  <Provider {...stores}>
    <Router>
      <Route path="/" component={Home} exact />
    </Router>
  </Provider>
, appDiv)
