const Web3 = require('web3')
const URL = require('url')
const express = require('express')
const app = express()
app.use(express.json())

app.use((_, res, next) => {
  res.set('Access-Control-Allow-Origin', '*')
  res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
  res.set('Access-Control-Allow-Headers', 'content-type')
  next()
})

app.get('/api', async (req, res) => {
  const { url } = req.query
  const { protocol } = URL.parse(url)
  let provider
  if (protocol === 'wss:' || protocol === 'ws:') {
    provider = new Web3.providers.WebsocketProvider(url)
  } else if (protocol === 'http:' || protocol === 'https:') {
    provider = new Web3.providers.HttpProvider(url)
  }
  const web3 = new Web3(provider)

  const [ currentBlock, syncState, networkId ] = await Promise.all([
    web3.eth.getBlockNumber(),
    web3.eth.isSyncing(),
    web3.eth.net.getId(),
  ])
  res.json({
    currentBlock,
    networkId,
    isSyncing: syncState === false ? false : true,
    ...syncState,
  })
})

module.exports = app
